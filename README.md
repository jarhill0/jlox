# jLox: Crafting Interpreters

Following along with [this](https://craftinginterpreters.com).

I've added custom extensions including:
- `input()` function to read from stdin
- `num()` function to convert a value to a number
- `%` operator for taking a modulus
- Assignment operators `+=`, `-=`, `*=`, `/=`, and `%=`
- Module imports
- String indexing
- `len()` to get the length of a sequence
- Arrays
- Python-style array multiplication and addition
- `hash()` to get the hash of an object
- `math` module with trig functions
