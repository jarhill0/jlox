package net.reeshill.lox;

import java.util.List;

abstract class Expr {
    abstract <T> T accept(Visitor<T> visitor);

    interface Visitor<T> {
        T visitBinaryExpr(Binary expr);

        T visitGroupingExpr(Grouping expr);

        T visitLiteralExpr(Literal expr);

        T visitUnaryExpr(Unary expr);

        T visitVariableExpr(Variable expr);

        T visitAssignExpr(Assign expr);

        T visitLogicalExpr(Logical expr);

        T visitCallExpr(Call expr);

        T visitGetExpr(Get expr);

        T visitSetExpr(Set expr);

        T visitThisExpr(This expr);

        T visitSuperExpr(Super expr);

        T visitIndexExpr(Index expr);

        T visitArrayExpr(Array expr);

        T visitSeqSetExpr(SeqSet expr);
    }

    static class Binary extends Expr {
        final Expr left;
        final Token operator;
        final Expr right;

        Binary(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitBinaryExpr(this);
        }
    }

    static class Grouping extends Expr {
        final Expr expression;

        Grouping(Expr expression) {
            this.expression = expression;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitGroupingExpr(this);
        }
    }

    static class Literal extends Expr {
        final Object value;

        Literal(Object value) {
            this.value = value;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitLiteralExpr(this);
        }
    }

    static class Unary extends Expr {
        final Token operator;
        final Expr right;

        Unary(Token operator, Expr right) {
            this.operator = operator;
            this.right = right;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitUnaryExpr(this);
        }
    }

    static class Variable extends Expr {
        final Token name;

        Variable(Token name) {
            this.name = name;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitVariableExpr(this);
        }
    }

    static class Assign extends Expr {
        final Token name;
        final Expr value;

        Assign(Token name, Expr value) {
            this.name = name;
            this.value = value;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitAssignExpr(this);
        }
    }

    static class Logical extends Expr {
        final Expr left;
        final Token operator;
        final Expr right;

        Logical(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitLogicalExpr(this);
        }
    }

    static class Call extends Expr {
        final Expr callee;
        final Token paren;
        final List<Expr> arguments;

        Call(Expr callee, Token paren, List<Expr> arguments) {
            this.callee = callee;
            this.paren = paren;
            this.arguments = arguments;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitCallExpr(this);
        }
    }

    static class Get extends Expr {
        final Expr object;
        final Token name;

        Get(Expr object, Token name) {
            this.object = object;
            this.name = name;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitGetExpr(this);
        }
    }

    static class Set extends Expr {
        final Expr object;
        final Token name;
        final Expr value;

        Set(Expr object, Token name, Expr value) {
            this.object = object;
            this.name = name;
            this.value = value;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitSetExpr(this);
        }
    }

    static class This extends Expr {
        final Token keyword;

        This(Token keyword) {
            this.keyword = keyword;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitThisExpr(this);
        }
    }

    static class Super extends Expr {
        final Token keyword;
        final Token method;

        Super(Token keyword, Token method) {
            this.keyword = keyword;
            this.method = method;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitSuperExpr(this);
        }
    }

    static class Index extends Expr {
        final Expr sequence;
        final Expr index;
        final Token bracket;

        Index(Expr sequence, Expr index, Token bracket) {
            this.sequence = sequence;
            this.index = index;
            this.bracket = bracket;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitIndexExpr(this);
        }
    }

    static class Array extends Expr {
        final List<Expr> elements;

        Array(List<Expr> elements) {
            this.elements = elements;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitArrayExpr(this);
        }
    }

    static class SeqSet extends Expr {
        final Expr sequence;
        final Expr index;
        final Expr value;
        final Token brack;

        SeqSet(Expr sequence, Expr index, Expr value, Token brack) {
            this.sequence = sequence;
            this.index = index;
            this.value = value;
            this.brack = brack;
        }

        <T> T accept(Visitor<T> visitor) {
            return visitor.visitSeqSetExpr(this);
        }
    }
}
