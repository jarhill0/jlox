package net.reeshill.lox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class UserInput implements LoxCallable {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader reader = new BufferedReader(input);

    @Override
    public Object call(List<Object> arguments) {
        try {
            return reader.readLine();
        } catch (IOException e) {
            // lol this shouldn't happen PLEASE
            throw new RuntimeException("oopsy whoopsy couldn't get input");
        }
    }

    @Override
    public int arity() {
        return 0;
    }

    @Override
    public String toString() {
        return "<native fn>";
    }
}
