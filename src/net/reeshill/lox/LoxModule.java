package net.reeshill.lox;

public class LoxModule {
    private final Interpreter interpreter;
    private final String name;

    public LoxModule(Interpreter interpreter, String name) {
        this.interpreter = interpreter;
        this.name = name;
    }

    Object get(Token name) {
        return interpreter.globals.get(name);
    }

    @Override
    public String toString() {
        return "module " + name;
    }

}
