package net.reeshill.lox;

import java.util.List;

public class NumFunction implements LoxCallable {
    @Override
    public String toString() {
        return "<native fn>";
    }

    @Override
    public Object call(List<Object> arguments) {
        Object arg = arguments.get(0);
        if (arg instanceof Double) {
            return arg;
        }
        if (arg instanceof Boolean) {
            return (Boolean) arg ? 1.0 : 0.0;
        }
        if (!(arg instanceof String)) {
            return null;
        }
        String str = (String) arg;
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public int arity() {
        return 1;
    }
}
