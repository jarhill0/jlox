package net.reeshill.lox;

import java.util.List;

public class MathModule extends LoxModule {
    private static Environment globals = new Environment();

    static {
        globals.define("cos", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof Double)
                    return Math.cos((double) argument);
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }
        });
        globals.define("sin", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof Double)
                    return Math.sin((double) argument);
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }
        });
        globals.define("tan", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof Double)
                    return Math.tan((double) argument);
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }
        });
        globals.define("acos", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof Double)
                    return Math.acos((double) argument);
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }
        });
        globals.define("asin", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof Double)
                    return Math.asin((double) argument);
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }
        });

        globals.define("atan", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof Double)
                    return Math.atan((double) argument);
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }
        });

        globals.define("atan2", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument1 = arguments.get(0);
                Object argument2 = arguments.get(1);
                if (argument1 instanceof Double && argument2 instanceof Double)
                    return Math.atan2((double) argument1, (double) argument2);
                return null;
            }

            @Override
            public int arity() {
                return 2;
            }
        });

        globals.define("pow", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument1 = arguments.get(0);
                Object argument2 = arguments.get(1);
                if (argument1 instanceof Double && argument2 instanceof Double)
                    return Math.pow((double) argument1, (double) argument2);
                return null;
            }

            @Override
            public int arity() {
                return 2;
            }
        });
    }

    public MathModule() {
        super(null, "math");
    }

    Object get(Token name) {
        return globals.get(name);
    }
}
