package net.reeshill.lox;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Interpreter implements Expr.Visitor<Object>, Stmt.Visitor<Void> {
    @SuppressWarnings("WeakerAccess")
    final Environment globals = new Environment();
    private final Map<Expr, Integer> locals = new HashMap<>();
    private final Map<String, LoxModule> knownModules = new HashMap<>();
    boolean isRepl = false;
    private Environment environment = globals;
    private final String filePath;

    Interpreter(String path) {
        filePath = path;
        globals.define("clock", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                return (double) System.currentTimeMillis() / 1000.0;
            }

            @Override
            public int arity() {
                return 0;
            }

            @Override
            public String toString() {
                return "<native fn>";
            }
        });
        globals.define("len", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                if (argument instanceof String) {
                    return (double) ((String) argument).length();
                }
                if (null == argument) {
                    return null;  // can't call getClass on null
                }
                if (argument.getClass().isArray()) {
                    return (double) ((Object[]) argument).length;
                }
                return null;
            }

            @Override
            public int arity() {
                return 1;
            }

            @Override
            public String toString() {
                return "<native fn>";
            }
        });
        globals.define("hash", new LoxCallable() {
            @Override
            public Object call(List<Object> arguments) {
                Object argument = arguments.get(0);
                return (double) argument.hashCode();
            }

            @Override
            public int arity() {
                return 1;
            }

            @Override
            public String toString() {
                return "<native fn>";
            }
        });
        globals.define("input", new UserInput());
        globals.define("num", new NumFunction());

        knownModules.put("math", new MathModule());
    }

    void interpret(List<Stmt> statements) {
        try {
            for (Stmt statement : statements) {
                execute(statement);
            }
        } catch (RuntimeError error) {
            Lox.runtimeError(error);
        }
    }

    private void execute(Stmt statement) {
        statement.accept(this);
    }

    void resolve(Expr expr, int depth) {
        locals.put(expr, depth);
    }

    private Object lookUpVariable(Token name, Expr expr) {
        Integer distance = locals.get(expr);
        if (distance != null) {
            return environment.getAt(distance, name.lexeme);
        } else {
            return globals.get(name);
        }
    }

    void executeBlock(List<Stmt> statements, Environment environment) {
        Environment previous = this.environment;

        try {
            this.environment = environment;

            for (Stmt statement : statements) {
                execute(statement);
            }
        } finally {
            this.environment = previous;
        }
    }

    @Override
    public Object visitBinaryExpr(Expr.Binary expr) {
        Object left = evaluate(expr.left);
        Object right = evaluate(expr.right);

        switch (expr.operator.type) {
            case MINUS:
                checkNumberOperands(expr.operator, left, right);
                return (double) left - (double) right;
            case SLASH:
                checkNumberOperands(expr.operator, left, right);
                return (double) left / (double) right;
            case STAR:
                switch (checkMultOperands(expr.operator, left, right)) {
                    case SCALAR:
                        return (double) left * (double) right;
                    case ARRAY_SCALAR:
                        return arrayScalarMult(left, right);
                }
            case PERCENT:
                checkNumberOperands(expr.operator, left, right);
            {
                double result = (double) left % (double) right;
                if (result < 0) {
                    result += (double) right; // [0, right). no negatives!
                }
                return result;
            }
            case PLUS:
                switch (checkAddOperands(expr.operator, left, right)) {
                    case STRING:
                        //noinspection RedundantCast
                        return (String) left + (String) right;
                    case ARRAY:
                        return concatArrays((Object[]) left, (Object[]) right);
                    case SCALAR:
                        return (double) left + (double) right;
                }
            case GREATER:
                checkNumberOperands(expr.operator, left, right);
                return (double) left > (double) right;
            case GREATER_EQUAL:
                checkNumberOperands(expr.operator, left, right);
                return (double) left >= (double) right;
            case LESS:
                checkNumberOperands(expr.operator, left, right);
                return (double) left < (double) right;
            case LESS_EQUAL:
                checkNumberOperands(expr.operator, left, right);
                return (double) left <= (double) right;
            case EQUAL_EQUAL:
                return isEqual(left, right);
            case BANG_EQUAL:
                return !isEqual(left, right);
        }

        return null;  // unreachable
    }

    private Object concatArrays(Object[] left, Object[] right) {
        Object[] newArr = new Object[left.length + right.length];
        System.arraycopy(left, 0, newArr, 0, left.length);
        System.arraycopy(right, 0, newArr, left.length, right.length);
        return newArr;
    }

    // Assumes one argument array and one double where double % 1 == 0
    private Object[] arrayScalarMult(Object left, Object right) {
        Object[] array;
        int multiplicand;
        if (left instanceof Double) {
            multiplicand = (int) ((double) left);
            array = (Object[]) right;
        } else {
            multiplicand = (int) ((double) right);
            array = (Object[]) left;
        }

        Object[] newArray = new Object[array.length * Math.max(multiplicand, 0)];
        for (int i = 0; i < multiplicand; i++) {
            System.arraycopy(array, 0, newArray, i * array.length, array.length);
        }
        return newArray;
    }

    @Override
    public Object visitGroupingExpr(Expr.Grouping expr) {
        return evaluate(expr.expression);
    }

    @Override
    public Object visitLiteralExpr(Expr.Literal expr) {
        return expr.value;
    }

    @Override
    public Object visitUnaryExpr(Expr.Unary expr) {
        Object right = evaluate(expr.right);

        switch (expr.operator.type) {
            case MINUS:
                checkNumberOperand(expr.operator, right);
                return -(double) right;
            case BANG:
                return !isTruthy(right);
        }

        // Unreachable
        return null;
    }

    @Override
    public Object visitVariableExpr(Expr.Variable expr) {
        return lookUpVariable(expr.name, expr);
    }

    @Override
    public Object visitAssignExpr(Expr.Assign expr) {
        Object value = evaluate(expr.value);
        Integer distance = locals.get(expr);
        if (distance != null) {
            environment.assignAt(distance, expr.name, value);
        } else {
            globals.assign(expr.name, value);
        }
        return value;
    }

    @Override
    public Object visitLogicalExpr(Expr.Logical expr) {
        Object left = evaluate(expr.left);

        if (expr.operator.type == TokenType.OR) {
            if (isTruthy(left)) return left;
        } else {
            if (!isTruthy(left)) return left;
        }
        return evaluate(expr.right);
    }

    @Override
    public Object visitCallExpr(Expr.Call expr) {
        Object callee = evaluate(expr.callee);

        List<Object> evaluatedArguments = new ArrayList<>();
        for (Expr argument : expr.arguments) {
            evaluatedArguments.add(evaluate(argument));
        }

        if (!(callee instanceof LoxCallable)) {
            throw new RuntimeError(expr.paren, "Can only call functions and " +
                    "classes.");
        }

        LoxCallable function = (LoxCallable) callee;
        if (function.arity() != evaluatedArguments.size()) {
            throw new RuntimeError(expr.paren,
                    String.format("Expected %d arguments but got %d.",
                            function.arity(), evaluatedArguments.size()));
        }

        return function.call(evaluatedArguments);
    }

    private Object evaluate(Expr expr) {
        return expr.accept(this);
    }

    private boolean isTruthy(Object object) {
        if (object == null) {
            return false;
        }
        if (object instanceof Boolean) {
            return (boolean) object;
        }
        return true;
    }

    private boolean isEqual(Object a, Object b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null) {
            return false;
        }
        if (a instanceof Double && b instanceof Double) {
            return (double) a == (double) b;  // else 0.0 != -0.0
        }
        return a.equals(b);
    }

    private void checkNumberOperand(Token operator, Object operand) {
        if (operand instanceof Double) {
            return;
        }
        throw new RuntimeError(operator, "Operand must be a number.");
    }

    private void checkNumberOperands(Token operator, Object left,
                                     Object right) {
        if (left instanceof Double && right instanceof Double) {
            return;
        }
        throw new RuntimeError(operator, "Operands must be numbers.");
    }

    private enum MultiplicationType {SCALAR, ARRAY_SCALAR}

    private MultiplicationType checkMultOperands(Token operator, Object left, Object right) {
        if (left instanceof Double && right instanceof Double) {
            return MultiplicationType.SCALAR;
        } else if (left.getClass().isArray() || right.getClass().isArray()) {
            double scalar;
            if (left instanceof Double) {
                scalar = (Double) left;
            } else if (right instanceof Double) {
                scalar = (Double) right;
            } else {
                throw new RuntimeError(operator, "Sequence can only be " +
                        "multiplied by a number.");
            }
            if (scalar % 1 != 0) {
                throw new RuntimeError(operator, "Sequence can only be " +
                        "multiplied by a whole number.");
            }
            return MultiplicationType.ARRAY_SCALAR;
        }
        throw new RuntimeError(operator, "Operands must be numbers.");
    }

    private enum AdditionType {SCALAR, STRING, ARRAY}

    private AdditionType checkAddOperands(Token operator, Object left,
                                          Object right) {
        if (left instanceof Double && right instanceof Double) {
            return AdditionType.SCALAR;
        } else if (left instanceof String && right instanceof String) {
            return AdditionType.STRING;
        } else if (left.getClass().isArray() && right.getClass().isArray()) {
            return AdditionType.ARRAY;
        }
        throw new RuntimeError(operator, "Operands must be numbers, strings, " +
                "or arrays.");
    }

    private String stringify(Object object) {
        if (object == null) {
            return "nil";
        }

        // Hack. Work around Java adding ".0" to integer-valued doubles.
        if (object instanceof Double) {
            if ((double) object % 1 == 0) {
                return String.format("%.0f", object);
            } else {
                return object.toString();
            }
        }

        if (object.getClass().isArray()) {
            Object[] array = (Object[]) object;
            StringBuilder builder = new StringBuilder("[");
            int i = 0;
            for (Object elem : array) {
                builder.append(stringify(elem));
                if (i++ != array.length - 1) {
                    builder.append(", ");
                }
            }
            return builder.append("]").toString();
        }

        return object.toString();
    }

    @Override
    public Void visitExpressionStmt(Stmt.Expression stmt) {
        Object value = evaluate(stmt.expression);
        if (isRepl) {
            System.out.println(stringify(value));
        }
        return null;
    }

    @Override
    public Void visitPrintStmt(Stmt.Print stmt) {
        Object value = evaluate(stmt.expression);
        System.out.println(stringify(value));
        return null;
    }

    @Override
    public Void visitVarStmt(Stmt.Var stmt) {
        Object value = null;
        if (stmt.initializer != null) {
            value = evaluate(stmt.initializer);
        }

        environment.define(stmt.name.lexeme, value);
        return null;
    }

    @Override
    public Void visitBlockStmt(Stmt.Block stmt) {
        executeBlock(stmt.statements, new Environment(environment));
        return null;
    }

    @Override
    public Void visitIfStmt(Stmt.If stmt) {
        if (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.thenBranch);
        } else if (stmt.elseBranch != null) {
            execute(stmt.elseBranch);
        }
        return null;
    }

    @Override
    public Void visitWhileStmt(Stmt.While stmt) {
        while (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.body);
        }
        return null;
    }

    @Override
    public Void visitFunctionStmt(Stmt.Function stmt) {
        LoxFunction function = new LoxFunction(stmt, environment, false, this);
        environment.define(stmt.name.lexeme, function);
        return null;
    }

    @Override
    public Void visitReturnStmt(Stmt.Return stmt) {
        Object value = null;
        if (stmt.value != null) {
            value = evaluate(stmt.value);
        }

        throw new Return(value);
    }

    @Override
    public Object visitSetExpr(Expr.Set expr) {
        Object object = evaluate(expr.object);

        if (!(object instanceof LoxInstance)) {
            throw new RuntimeError(expr.name, "Only instances are settable.");
        }

        Object value = evaluate(expr.value);
        ((LoxInstance) object).set(expr.name, value);
        return value;
    }

    @Override
    public Object visitGetExpr(Expr.Get expr) {
        Object object = evaluate(expr.object);
        if (object instanceof LoxInstance) {
            return ((LoxInstance) object).get(expr.name);
        } else if (object instanceof LoxModule) {
            return ((LoxModule) object).get(expr.name);
        }

        throw new RuntimeError(expr.name, "Only instances and modules have " +
                "properties.");
    }

    @Override
    public Object visitThisExpr(Expr.This expr) {
        return lookUpVariable(expr.keyword, expr);
    }

    @Override
    public Object visitSuperExpr(Expr.Super expr) {
        int distance = locals.get(expr);
        LoxClass superclass = (LoxClass) environment.getAt(distance, "super");

        // "this" is always one level closer than "super"
        LoxInstance object = (LoxInstance) environment.getAt(distance - 1,
                "this");

        LoxFunction method = superclass.findMethod(expr.method.lexeme);

        if (method == null) {
            throw new RuntimeError(expr.method, String.format("Undefined " +
                    "property '%s'", expr.method.lexeme));
        }

        return method.bind(object);
    }

    @Override
    public Object visitIndexExpr(Expr.Index expr) {
        return index(evaluate(expr.sequence), evaluate(expr.index), expr.bracket);
    }

    @Override
    public Object visitArrayExpr(Expr.Array arrayExp) {
        Object[] array = new Object[arrayExp.elements.size()];
        int i = 0;
        for (Expr element : arrayExp.elements) {
            array[i++] = evaluate(element);
        }
        return array;
    }

    @Override
    public Object visitSeqSetExpr(Expr.SeqSet expr) {
        Object sequence = evaluate(expr.sequence);
        if (!sequence.getClass().isArray()) {
            throw new RuntimeError(expr.brack, "Can only set elements of an " +
                    "array");
        }
        Object[] array = (Object[]) sequence;
        int index = computeRealIndex(evaluate(expr.index), array.length,
                expr.brack);
        Object value = evaluate(expr.value);
        array[index] = value;
        return value;
    }

    private int computeRealIndex(Object index, int length, Token bracket) {
        if (!(index instanceof Double)) {
            throw new RuntimeError(bracket, "Indexes must be numbers.");
        }
        double ind = (Double) index;
        if (ind % 1 != 0) {
            throw new RuntimeError(bracket, "Indexes must be integers.");
        }
        int trueIndex = (int) ind;

        if (trueIndex < -length || trueIndex >= length) {
            throw new RuntimeError(bracket,
                    String.format("Index %d out of range.", trueIndex));
        }
        trueIndex = trueIndex < 0 ? trueIndex + length : trueIndex;
        return trueIndex;
    }

    private Object index(Object sequence, Object index, Token bracket) {
        if (sequence instanceof String) {
            String string = (String) sequence;
            int trueIndex = computeRealIndex(index, string.length(), bracket);
            return string.substring(trueIndex, trueIndex + 1);
        }
        if (null != sequence && sequence.getClass().isArray()) {
            Object[] array = (Object[]) sequence;
            return array[computeRealIndex(index, array.length, bracket)];
        }
        throw new RuntimeError(bracket, "Not indexable.");
    }

    @Override
    public Void visitClassStmt(Stmt.Class stmt) {
        Object superclass = null;
        if (stmt.superclass != null) {
            superclass = evaluate(stmt.superclass);
            if (!(superclass instanceof LoxClass)) {
                throw new RuntimeError(stmt.superclass.name, "Superclass must" +
                        " be a class.");
            }
        }

        environment.define(stmt.name.lexeme, null);

        if (stmt.superclass != null) {
            environment = new Environment(environment);
            environment.define("super", superclass);
        }

        Map<String, LoxFunction> methods = new HashMap<>();
        for (Stmt.Function method : stmt.methods) {
            LoxFunction function = new LoxFunction(method, environment,
                    method.name.lexeme.equals("init"), this);
            methods.put(method.name.lexeme, function);
        }

        LoxClass klass = new LoxClass(stmt.name.lexeme, (LoxClass) superclass
                , methods);
        if (superclass != null) {
            environment = environment.enclosing;
        }
        environment.assign(stmt.name, klass);
        return null;
    }

    @Override
    public Void visitImportStmt(Stmt.Import stmt) {
        String moduleName = stmt.name.lexeme;
        if (knownModules.containsKey(moduleName)) {
            globals.define(moduleName, knownModules.get(moduleName));
            return null;
        }

        String[] pathParts =
                new File(filePath).getAbsolutePath().split(File.separator);
        pathParts[pathParts.length - 1] = moduleName;
        String fileName = String.join(File.separator, pathParts);

        Interpreter fileInterpreter = null;
        try {
            fileInterpreter = Lox.getFileInterpreter(fileName);
        } catch (IOException e) {
            fileName += ".lox";
            try {
                fileInterpreter = Lox.getFileInterpreter(fileName);
            } catch (IOException g) {
                Lox.error(stmt.name,
                        "Could not open/read module '" + stmt.name.lexeme +
                                "'.");
            }
        }

        if (fileInterpreter == null) {
            throw new RuntimeError(stmt.name, "Runtime error in parsing " +
                    "import.");
        }

        globals.define(stmt.name.lexeme,
                new LoxModule(fileInterpreter, stmt.name.lexeme));
        return null;
    }
}
