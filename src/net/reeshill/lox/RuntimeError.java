package net.reeshill.lox;

@SuppressWarnings("WeakerAccess")
public class RuntimeError extends RuntimeException {
    final Token token;

    RuntimeError(Token token, String message) {
        super(message);
        this.token = token;
    }
}
