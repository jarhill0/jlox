package net.reeshill.lox;

import java.util.List;

interface LoxCallable {
    Object call(List<Object> arguments);

    int arity();
}
