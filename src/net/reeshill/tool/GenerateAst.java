package net.reeshill.tool;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class GenerateAst {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: generate_ast <directory>");
            System.exit(1);
        }
        String outputDir = args[0];
        defineAst(outputDir, "Expr", Arrays.asList(
                "Binary   : Expr left, Token operator, Expr right",
                "Grouping : Expr expression",
                "Literal  : Object value",
                "Unary    : Token operator, Expr right",
                "Variable : Token name",
                "Assign   : Token name, Expr value",
                "Logical  : Expr left, Token operator, Expr right",
                "Call     : Expr callee, Token paren, List<Expr> arguments",
                "Get      : Expr object, Token name",
                "Set      : Expr object, Token name, Expr value",
                "This     : Token keyword",
                "Super    : Token keyword, Token method",
                "Index    : Expr sequence, Expr index, Token bracket",
                "Array    : List<Expr> elements",
                "SeqSet   : Expr sequence, Expr index, Expr value, Token brack"
        ));
        defineAst(outputDir, "Stmt", Arrays.asList(
                "Expression : Expr expression",
                "Print      : Expr expression",
                "Var        : Token name, Expr initializer",
                "Block      : List<Stmt> statements",
                "If         : Expr condition, Stmt thenBranch, Stmt elseBranch",
                "While      : Expr condition, Stmt body",
                "Function   : Token name, List<Token> params, List<Stmt> body",
                "Return     : Token keyword, Expr value",
                "Class      : Token name, Expr.Variable superclass, List<Stmt" +
                        ".Function> methods",
                "Import     : Token name"
        ));
    }

    private static void defineAst(String outputDir, String className,
                                  List<String> types) throws IOException {
        String path = String.format("%s/%s.java", outputDir, className);
        PrintWriter writer = new PrintWriter(path, "UTF-8");
        writer.println("package net.reeshill.lox;");
        writer.println();
        writer.println("import java.util.List;");
        writer.println();
        writer.println(String.format("abstract class %s {", className));
        defineVisitor(writer, className, types);
        writer.println();
        writer.println("    abstract <T> T accept(Visitor<T> visitor);");
        writer.println();
        for (String subclass : types) {
            String subclassName = subclass.split(":")[0].trim();
            String subclassFields = subclass.split(":")[1].trim();
            defineType(writer, className, subclassName, subclassFields);
        }
        writer.println("}");
        writer.close();
    }

    private static void defineType(PrintWriter writer, String parentName,
                                   String className, String fields) {
        writer.println(String.format("    static class %s extends %s {",
                className, parentName));
        String[] fieldList = fields.split(", ");

        // Fields
        for (String field : fieldList) {
            writer.println(String.format("        final %s;", field));
        }
        writer.println();

        // Constructor
        writer.println(String.format("        %s(%s) {", className, fields));
        for (String field: fieldList) {
            String name = field.split(" ")[1];
            writer.println(String.format("            this.%s = %s;", name,
                    name));
        }
        writer.println("        }");
        writer.println();

        writer.println("        <T> T accept(Visitor<T> visitor) {");
        writer.println(String.format("            return visitor.visit%s%s" +
                "(this);", className, parentName));
        writer.println("        }");

        writer.println("    }");
    }

    private static void defineVisitor(PrintWriter writer, String baseName,
                                      List<String> types) {
        writer.println("    interface Visitor<T> {");
        for (String type: types) {
            String typeName = type.split(":")[0].trim();
            writer.println(String.format("        T visit%s%s(%s %s);",
                    typeName, baseName, typeName, baseName.toLowerCase()));
        }
        writer.println("    }");
    }
}
